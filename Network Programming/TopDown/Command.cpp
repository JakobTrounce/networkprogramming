#include "Command.h"

void Command::SetDuration(float Duration)
{
	mDuration = Duration;
}

float Command::GetCommandTime(float deltaTime)
{
	return X::Math::Min(mDuration, deltaTime);
}

void Command::Update(float deltaTime)
{
	mDuration -= deltaTime;
	if (mDuration <= 0.0f)
	{
		mDuration = 0.0f;
		mTimedCommand = 0;
	}

	//clear oneshots
	mOneShotCommand = 0;
}
bool Command::Empty() const
{
	return mTimedCommand == 0 && mOneShotCommand == 0;
}

bool Command::MoveForward() const
{
	return (mTimedCommand& kUp) != 0;
}

bool Command::MoveBackward() const
{
	return (mTimedCommand & kDown) != 0;
}

bool Command::MoveRight() const
{

	return (mTimedCommand & kRight) != 0;
}

bool Command::MoveLeft() const
{

	return (mTimedCommand & kLeft) != 0;
}

void Command::Serialize(Network::StreamWriter& writer)
{
	writer.Write(mDuration);
	writer.Write(mTimedCommand);
	writer.Write(mOneShotCommand);
}

void Command::Deserialize(Network::StreamReader& reader)
{
	reader.Read(mDuration);
	reader.Read(mTimedCommand);
	reader.Read(mOneShotCommand);
}

