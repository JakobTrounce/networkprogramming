#ifndef INCLUDED_CHARACTER_H
#define INCLUDED_CHARACTER_H

#include "Command.h"
#include "GameObject.h"

class Character : public GameObject
{
public:
	CLASS_ID(Character, 'CHAR');
	Character();

	void Load() override;
	void Update(float deltaTime) override;
	void Draw() override;

	void Serialize(Network::StreamWriter& writer) const override;
	void Deserialize(Network::StreamReader& reader) override;

	void Apply(const Command& command) { mCommand = command; }

	void SetPosition(const X::Math::Vector2& pos) { mPosition = pos; }
	const X::Math::Vector2 GetPosition() const { return mPosition; }

protected:
	Command mCommand;
	X::Math::Vector2 mPosition;
	X::TextureId mTextureId;
};



#endif