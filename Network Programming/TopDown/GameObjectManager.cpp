#include "GameObjectManager.h"

namespace
{
	GameObjectManager* sInstance = nullptr;
}

void GameObjectManager::StaticInitialize()
{
	XASSERT(sInstance == nullptr, "[GameObjectManager] Manager already initialized");
	sInstance = new GameObjectManager();
}

void GameObjectManager::StaticTerminate()
{
	XASSERT(sInstance != nullptr, "[GameObjectManager] Manager already Terminated");
	X::SafeDelete(sInstance);
}

GameObjectManager* GameObjectManager::Get()
{
	XASSERT(sInstance != nullptr, "[GameObjectManager] Manager Not initialized");
	return sInstance;
}


GameObjectManager::GameObjectManager()
{

}

GameObjectManager::~GameObjectManager()
{
	for (size_t i = 0; i < mGameObjects.size(); ++i)
	{
		X::SafeDelete(mGameObjects[i]);
	}
	mGameObjects.clear();
}

Network::ObjectFactory& GameObjectManager::GetFactory()
{
	return mObjectFactory;
}

GameObject* GameObjectManager::CreateGameObject(uint32_t type)
{
	GameObject* newObject = (GameObject*)mObjectFactory.CreateInstance(type);
	mGameObjects.push_back(newObject);

	newObject->mNetworkId = mLinkingContext.Register(newObject);
	return newObject;
}

GameObject* GameObjectManager::CreateGameObject(Network::StreamReader& reader)
{
	uint32_t type = 0;
	uint32_t id = 0;
	reader.Read(type);
	reader.Read(id);
	GameObject* newObject = (GameObject*)mObjectFactory.CreateInstance(type);
	mGameObjects.push_back(newObject);
	mLinkingContext.Register(newObject, id);
	newObject->Load();
	newObject->Deserialize(reader);
	return newObject;
}

void GameObjectManager::DestroyGameObject(uint32_t networkId)
{
	auto iter = std::find_if(mGameObjects.begin(), mGameObjects.end(), [networkId](GameObject* obj) {return obj->GetNetworkId() == networkId; });
	if (iter != mGameObjects.end())
	{
		(*iter)->Kill();
	}
}

void GameObjectManager::Serialize(Network::StreamWriter& writer, bool dirtyOnly) const
{
	//count objects that need to be synced

	GameObjectList gameObjectsToSynchronize;
	gameObjectsToSynchronize.reserve(mGameObjects.size());

	for (auto gameObject : mGameObjects)
	{
		if (gameObject->IsSync() && (!dirtyOnly || gameObject->IsDirty()))
		{
			gameObjectsToSynchronize.emplace_back(gameObject);
		}
	}

	//serialize object states

	writer.Write(gameObjectsToSynchronize.size());
	for (auto gameObject : gameObjectsToSynchronize)
	{
		writer.Write(gameObject->GetClassId());
		writer.Write(gameObject->GetNetworkId());
		gameObject->Serialize(writer);
		gameObject->Clean();
	}
}

void GameObjectManager::Deserialize(Network::StreamReader& reader)
{
	XASSERT(mGameObjects.empty(), "[GameObjectManager] Manager is not empty");

	size_t count = 0;
	reader.Read(count);

	for (size_t i = 0; i < count; ++i)
	{
		CreateGameObject(reader);
	}
}

void GameObjectManager::Update(float deltaTime)
{
	for (auto gameObject : mGameObjects)
	{
		if (gameObject->IsActive())
		{
			gameObject->Update( deltaTime);
		}
	}
	for (size_t i = 0; i < mGameObjects.size();)
	{
		GameObject* gameObject = mGameObjects[i];
		if (!gameObject->IsActive())
		{
			mLinkingContext.Unregister(gameObject);
			X::SafeDelete(gameObject);
			mGameObjects[i] = mGameObjects.back();
			mGameObjects.pop_back();
		}
		else
		{
			++i;
		}
	}
}

void GameObjectManager::Update(Network::StreamReader& reader)
{
	size_t count = 0;
	reader.Read(count);

	for (size_t i = 0; i < count; ++i)
	{
		uint32_t type = 0;
		uint32_t id = 0;
		reader.Read(type);
		reader.Read(id);

		GameObject* newObject = (GameObject*)mLinkingContext.GetInstance(id);
		XASSERT(newObject != nullptr, "[GameObjectManager] Object %d not found");
		newObject->Deserialize(reader);
		/* 
		if(newObject!= nullptr)
		{
		newObject->Deserialize(reader);
		}
		else
		{
		CreateGameObject(reader);
		}
		
		*/
	}
}


void GameObjectManager::Draw()
{
	for (auto gameObject : mGameObjects)
	{
		gameObject->Draw();
	}
}

GameObject* GameObjectManager::FindGameObject(uint32_t networkId)
{
	auto iter = std::find_if(mGameObjects.begin(), mGameObjects.end(),
		[networkId](GameObject* obj) { return obj->GetNetworkId() == networkId; });
	if (iter != mGameObjects.end())
	{
		return *iter;
	}
	return nullptr;
}