#include "ServerNetworkManager.h"

const uint16_t port = 8888;

int main(int argc, char* argv[])
{
//startup
	GameObjectManager::StaticInitialize();
	ServerNetworkManager::StaticInitialize();

	//register objject types
	GameObjectManager::Get()->GetFactory().Register<Character>();

	//server forever
	ServerNetworkManager::Get()->Run(port);

	//shutdown

	GameObjectManager::StaticTerminate();
	ServerNetworkManager::StaticTerminate();
}