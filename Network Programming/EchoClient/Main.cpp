#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <WinSock2.h>
#include <iostream>
#include <cstdio>
#include <cstdint>
#include <string>
#pragma comment(lib,"Ws2_32.lib")

int main(int argc, char* argv[])
{
	const char* hostAddress = "127.0.0.1";
	uint16_t port = 8888;

	//initialize Winsock version 2,2
	WSAData wsaData;
	WSAStartup(MAKEWORD(2, 2), &wsaData);

	SOCKET mySocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	//Resolve host using address
	in_addr iaHost;
	iaHost.s_addr = inet_addr(hostAddress);
	HOSTENT* hostEntry = gethostbyaddr((const char*)&iaHost, sizeof(struct in_addr), AF_INET);

	//fill host address information and connect
	SOCKADDR_IN serverInfo;
	serverInfo.sin_family = AF_INET;
	serverInfo.sin_addr = *((LPIN_ADDR)*hostEntry->h_addr_list);
	serverInfo.sin_port = htons(port);
	connect(mySocket,(LPSOCKADDR)&serverInfo, sizeof(struct sockaddr));

	printf("Connected to server\n");

	std::string message;
	std::cout << "Enter a message: ";
	std::cin >> message;
	printf("Sending: %s\n", message.c_str());
	//send user message
	int bytesSent = send(mySocket, message.c_str(), (int)message.length(), 0);

	//wait for server response **blocking**
	char buffer[1024];
	// send and recieve bytes, not strings
	int bytesReceived = recv(mySocket, buffer, std::size(buffer) - 1, 0);
	if (bytesReceived == SOCKET_ERROR)
	{
		printf("recv failed.\n");
		return -1;
	}
	if (bytesReceived == 0)
	{
		printf("Connection closed.\n");
		return 0;
	}
	else
	{
		buffer[bytesReceived] = '\0';
		printf("Received: %s\n", buffer);
	}

	//close all sockets
	closesocket(mySocket);
	//Shutdown Winsock
	WSACleanup();
	return 0;
}