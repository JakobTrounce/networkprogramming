#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <WinSock2.h>
#include <iostream>
#include <cstdio>
#include <cstdint>
#include <string>
#include <vector>
#pragma comment(lib,"Ws2_32.lib")

void ParseString( char* names,std::vector<std::string> fileNames)
{
	char* token = nullptr;
	char* name = strtok_s(names, ",", &token);
	while (name != nullptr)
	{
		fileNames.push_back(names);
		names = strtok_s(nullptr, ",",&token);
	}
}
int main(int argc, char* argv[])
{
	std::vector<std::string> fileNames;
	FILE* file;
	const char* hostAddress = "127.0.0.1";
	uint16_t port = 8888;

	//initialize Winsock version 2,2
	WSAData wsaData;
	WSAStartup(MAKEWORD(2, 2), &wsaData);

	SOCKET mySocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	//Resolve host using address
	in_addr iaHost;
	iaHost.s_addr = inet_addr(hostAddress);
	HOSTENT* hostEntry = gethostbyaddr((const char*)&iaHost, sizeof(struct in_addr), AF_INET);

	//fill host address information and connect
	SOCKADDR_IN serverInfo;
	serverInfo.sin_family = AF_INET;
	serverInfo.sin_addr = *((LPIN_ADDR)*hostEntry->h_addr_list);
	serverInfo.sin_port = htons(port);
	connect(mySocket, (LPSOCKADDR)&serverInfo, sizeof(struct sockaddr));

	printf("Connected to server\n");

	std::cout << "Type 'list' for list of files: " << std::endl;
	std::cout << "Or enter the name of the file you want to download: " << std::endl;

	std::string message;
	std::cin >> message;

	int bytesSent = send(mySocket, message.c_str(), (int)message.length(), 0);
	//wait for server response **blocking**
	char buffer[1024];
	// send and recieve bytes, not strings;
	std::vector<char> receivedBuffer;
		int bytesReceived = recv(mySocket, buffer, std::size(buffer) - 1, 0);
		if (bytesReceived == SOCKET_ERROR)
		{
			printf("recv failed.\n");
			return -1;
		}
		if (bytesReceived == 0)
		{
		}
		buffer[bytesReceived] = '\0';
		printf("%s\n", buffer);
			std::cin >> message;
			fopen_s(&file, message.c_str(), "wb");
		 bytesSent = send(mySocket, message.c_str(), (int)message.length(), 0);

		while (true)
		{
			bytesReceived = recv(mySocket, buffer, std::size(buffer) - 1, 0);
		
		if (bytesReceived == SOCKET_ERROR)
		{
			printf("recv failed.\n");
			return -1;
		}
		if ( bytesReceived == 0)
		{
			printf("Connection closed.\n");
			break;
		}
			receivedBuffer.insert(receivedBuffer.end(), buffer, buffer + bytesReceived);
	}
	if (!receivedBuffer.empty()) 
	{

		fwrite(receivedBuffer.data(), 1, receivedBuffer.size(), file);
	}
	fclose(file);
	//close all sockets
	closesocket(mySocket);
	//Shutdown Winsock
	WSACleanup();
	return 0;
}