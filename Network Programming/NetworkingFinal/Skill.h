#ifndef INCLUDED_SKILL_H
#define INCLUDED_SKILL_H
#include <XEngine.h>
class Skill
{
public:
	virtual void Activate() = 0;

protected:
	float mDuration{0};
	float mCooldown{0};
};


#endif
