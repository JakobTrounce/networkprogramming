#include "Projectile.h"
#include "GameManager.h"
Projectile::Projectile(std::string textureName)
	: mTextureName(textureName)
{

}

Projectile::Projectile()
{
	mTextureName = "bullet1.png";
}

Projectile::~Projectile()
{

}

void Projectile::Load()
{
	mTexture = X::LoadTexture(mTextureName.c_str());
	ProjectileWidth = X::GetSpriteWidth(mTexture);
	ProjectileHeight = X::GetSpriteHeight(mTexture);
}

void Projectile::Unload()
{
	mTexture = 0;
}

void Projectile::Update(float deltaTime)
{

	if (IsOutOfBounds() || IsColliding())
	{
		mActive = false;
	}
	if (IsActive())
	{
		mPosition += mVelocity *mSpeed * deltaTime;
	}
}

void Projectile::Render()
{
	if (IsActive())
	{
		X::DrawSprite(mTexture, mPosition);
		if (IsDebugging())
		{
			auto circle = GetBoundingCircle();
			X::DrawScreenCircle(circle.center, circle.radius, X::Math::Vector4::White());
		}
	}
}

void Projectile::Fire(const X::Math::Vector2& pos, const X::Math::Vector2& vel)
{
	mPosition = { pos };
	mVelocity = vel;
	mActive = true;
}

bool Projectile::IsOutOfBounds()
{
	
	if (mPosition.y < 0)
	{
		return true;
	}
	if (mPosition.y > GameManager::Get()->ScreenHeight())
	{
		return true;
	}
	if (mPosition.x < 0)
	{
		return true;
	}
	if (mPosition.x > GameManager::Get()->ScreenWidth())
	{
		return true;
	}

	return false;
}

void Projectile::Kill()
{
	mActive = false;
}

X::Math::Circle Projectile::GetBoundingCircle()
{
	X::Math::Circle c;
	c.center = mPosition;
	c.radius = ProjectileWidth * 0.5f;

	return c;

}


bool Projectile::IsColliding()
{
	auto circle = GetBoundingCircle();

	// (x2-x1)^2 + (y1-y2)^2 <= (r1+r2)^2

	return false;
}

