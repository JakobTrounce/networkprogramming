#include "Precompiled.h"
#include "UDPSocket.h"

Network::UDPSocket::UDPSocket()
	: mSocket(INVALID_SOCKET)
{
}

Network::UDPSocket::~UDPSocket()
{
	Close();
}

bool Network::UDPSocket::Open()
{
	//ASSERT(mSocket == INVALID_SOCKET,"[UDPSocket] Socket already opened")
	mSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (mSocket == INVALID_SOCKET)
	{
		//LOG("[UDPSocket] Failed to open Socket")
		return false;
	}
	return true;
}

void Network::UDPSocket::Close()
{
	if (mSocket != INVALID_SOCKET)
	{
		closesocket(mSocket);
		mSocket = INVALID_SOCKET;
	}

}

bool Network::UDPSocket::Bind(const SocketAddress & fromAddress)
{
	if (mSocket == INVALID_SOCKET && !Open())
	{
		return false;
	}

	int result = bind(mSocket, &fromAddress.mSockAddr, fromAddress.GetSize());
	if (result == SOCKET_ERROR)
	{
		//LOG([UDPSocket] Failed to bind socket. Error: %d",WSAGetLastError)
		return false;
	}
	return true;
}

bool Network::UDPSocket::SetBroadcast(bool broadcast)
{
	//for homework
	//SetSockopt
	return false;
}

int Network::UDPSocket::SendTo(const void * buffer, int len, const SocketAddress & toAddress)
{
	//ASSERT(len >0,"[UDPSocket] Invalid Buffer length.")
	if (mSocket == INVALID_SOCKET && !Open())
	{
		return SOCKET_ERROR;
	}

	int bytesSent = sendto(mSocket, static_cast<const char*>(buffer), len, 0, &toAddress.mSockAddr, toAddress.GetSize());
		if (bytesSent < 0)
		{
			//LOG("[UDPSocket] Failed to Send data. Error: %d", WSAGetLastError())
			return SOCKET_ERROR;
		}
	return bytesSent;
}

int Network::UDPSocket::ReceiveFrom(void * buffer, int len, SocketAddress & fromAddress)
{
	int fromLength = fromAddress.GetSize();
	int bytesRead = recvfrom(mSocket, static_cast<char*>(buffer), len, 0, &fromAddress.mSockAddr, &fromLength);
	if (bytesRead >= 0)
	{
		return bytesRead;
	}
	else
	{
		int error = WSAGetLastError();
		if (error == WSAEWOULDBLOCK)
		{
			return 0;
		}
		else if (error == WSAECONNRESET)
		{
			return -WSAECONNRESET;
		}
		else
		{
			return -error;
		}
	}
}
