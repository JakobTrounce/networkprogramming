#include "Precompiled.h"
#include "StreamReader.h"

using namespace Network;

StreamReader::StreamReader(MemoryStream& memoryStream)
	:mMemoryStream(memoryStream)
{

}

void StreamReader::Read(const void* data, uint32_t size)
{
	
	std::memcpy(&data, mMemoryStream.mBuffer + mMemoryStream.mHead, size);
	mMemoryStream.mHead += size;
}

void StreamReader::Read(std::string& str)
{
	const size_t length = str.length();
	Read(length);
	Read(&str[0], length);
}